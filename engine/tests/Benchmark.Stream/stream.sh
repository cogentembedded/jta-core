tarball=stream.tar.bz2

function test_build {
	make stream_c.exe CFLAGS+="${CFLAGS}" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put stream_c.exe  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; ./stream_c.exe"  
}

. $JTA_SCRIPTS_PATH/benchmark.sh
