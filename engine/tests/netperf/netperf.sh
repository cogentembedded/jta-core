tarball=netperf-2.6.0.tar.bz2

source $JTA_SCRIPTS_PATH/overlays.sh
set_overlay_vars

source $JTA_SCRIPTS_PATH/reports.sh
source $JTA_SCRIPTS_PATH/functions.sh


TESTDIR="netperf"
TEST_HOME="$JTA_TESTS_PATH/netperf"
TRIPLET=$TESTDIR-$PLATFORM

function test_build {
    patch -p0 < ../../tarballs/wait_before_data.patch
    echo "ac_cv_func_setpgrp_void=yes" > config.cache
    ./configure --build=`./config.guess` --host=$HOST CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" --config-cache
    make && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	cd doc/examples
	cp $TEST_HOME/netperf-rabench_script ./rabench_script
	cp $TEST_HOME/netperf-random_rr_script ./random_rr_script
	chmod a+x *script;
	put *script  $JTA_HOME/jta.$TESTDIR/
	cd $WORKSPACE/$TRIPLET/src/
	put netperf  $JTA_HOME/jta.$TESTDIR/
}

if [ "$type" = "Benc" ]; then 
    if [ "$BENCHMARK_NETPERF_SRV" = "default" ]; then
      srv=$SRV_IP
    else 
      srv=$BENCHMARCK_NETPERF_SRV
    fi
else 
    if [ "$FUNCTIONAL_NETPERF_SRV" = "default" ]; then
      srv=$SRV_IP
    else 
      srv=$FUNCTIONAL_NETPERF_SRV
    fi
fi


pre_test $TESTDIR

if $Rebuild; then
    build
fi

deploy


