tarball=dung-3.4.25-m2.tar.gz

function test_build  {
    touch test_suit_ready
}

function test_deploy {
    put -r ./* $OSV_HOME/osv.$TESTDIR/
}

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR/cmt; ./cmt-interrupt.sh; ./dmesg.sh; ./proc-interrupts.sh"
}

function test_processing {
    check_capability "RENESAS"

    log_compare "$TESTDIR" $FUNCTIONAL_CMT_LINES_COUNT "Test passed" "p"
}

. ../scripts/functional.sh

