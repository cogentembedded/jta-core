source $JTA_SCRIPTS_PATH/overlays.sh
set_overlay_vars

source $JTA_SCRIPTS_PATH/reports.sh
source $JTA_SCRIPTS_PATH/functions.sh

source $TEST_HOME/../LTP/ltp.sh

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR; mkdir tmp; ./runltp -d tmp -f fs_jta"  
}

test_run
get_testlog $TESTDIR
