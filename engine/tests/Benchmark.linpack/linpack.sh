tarball=linpack.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/linpack.c.patch
    $CC $CFLAGS -O -lm -o linpack linpack.c && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put linpack  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR && ./linpack"  
}

. $JTA_SCRIPTS_PATH/benchmark.sh
