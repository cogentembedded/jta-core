tarball=dung-3.4.25-m2.tar.gz

function test_build  {
    touch test_suit_ready
}

function test_deploy {
    put -r ./* $OSV_HOME/osv.$TESTDIR/
}

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR/sdhi.0; ./unbind-bind-write.sh; ./write-10M.sh; ./write-1k.sh"
}

function test_processing {
    assert_define FUNCTIONAL_SDHI_0_RES_LINES_COUNT

    check_capability "RENESAS"

    log_compare "$TESTDIR" ""FUNCTIONAL_SDHI_0_RES_LINES_COUNT "Test passed" "p"

}

. $JTA_SCRIPTS_PATH/functional.sh

